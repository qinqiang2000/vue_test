// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

Vue.filter('dCurrency', function (value) {
  return '￥' + value
})
Vue.filter('dTofixed', function (value) {
  var isNum = parseFloat(value)
  if (!isNum) {
    alert('请输入数字')
    return '请输入数字'
  } else {
    var val = Math.round(isNum * 100) / 100
    var item = val.toString().split('.')
    if (item.length === 1) {
      val = val.toString() + '.00'
      return val
    }
    if (item.length > 1) {
      if (item[1].length < 2) {
        val = val.toString() + '0'
      }
      return val
    }
  }
})