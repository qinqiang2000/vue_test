import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Detail from '@/components/GoodsDetail'
import Msg from '@/components/Message'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/detail/:title',
      component: Detail,
      name: "detail",
      children: [
        {
          path: 'msg',
          component: Msg
        }
      ]
    }
  ]
})
